#!/bin/bash
#
# Highly-opinionated script to build OpenCV for four endpoints:
#
#   - NVidia Jetson NX / Jetpack 4.6.x / L4T 32.x.x for the trisect Gen1 project
#   - NVidia Orin NX / Jetpack 5.1.x / L4T 35.x.x for the trisect Gen2 project
#   - arm64 Docker (to emulate the Trisect)
#   - amd64 Docker
#
# Based on a script of the same name from JetsonHacks
#
# License: MIT. See license file in root directory
# Copyright(c) University of Washington 2023

#!!  For simplicity, these defaults are for bare metal
#!!  on Trisect.   We can override them in the Dockerfiles.

# Default OpenCV tag / release.   Can be overridden with --opencv-version
OPENCV_VERSION=${OPENCV_VERSION:-"4.8.0"}

# This script installs OpenCV with Stow
# The default install location is /usr/local/stow/opencv-x.x.x/
# Which is stowed to /usr/local/
INSTALL_DIR=""
DO_INSTALL=YES
SOURCE_DIR=$PWD/build

# Download the opencv_extras repository
DOWNLOAD_OPENCV_EXTRAS=NO

## Only use sudo if necessary
SUDO=''
if (( $EUID != 0 )); then
    SUDO='sudo'
fi

function usage
{
    echo "usage: ./buildOpenCV.sh [[-s sourcedir ] | [-h]]"
    echo "--platform         Platform {jetpack,docker-amd64}"
    echo "-s | --sourcedir   Directory in which to place the opencv sources (default $SOURCE_DIR)"
    echo "-i | --installdir  Directory in which to install opencv libraries (default $INSTALL_DIR)"
    echo "--opencv-version   OpenCV git tag / version to build (default $OPENCV_VERSION)"
    echo "--no-install       Don't install OpenCV after building"
    echo "--package          Build installer packages .deb"
    echo "-h | --help  This message"
}

# Iterate through command line inputs
while [ "$1" != "" ]; do
    case $1 in
        --platform )            shift
                                PLATFORM=$1
                                ;;
        -s | --sourcedir )      shift
                                SOURCE_DIR=$1
                                ;;
        -i | --installdir )     shift
                                INSTALL_DIR=$1
                                ;;
        --package )             PACKAGE_OPENCV="-D CPACK_BINARY_DEB=ON"
                                ;;
        --no-install )          DO_INSTALL=NO
                                ;;
        --opencv-version )      shift
                                OPENCV_VERSION=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ "$PLATFORM" == "" ]; then
  echo "Platform not defined. Options:  jetpack, docker-amd64"
  exit -1
fi

# If not other specified, set a reasonable default
if [ "${INSTALL_DIR}" == "" ] && [ "${DO_INSTALL}" == "YES" ]; then
  INSTALL_DIR=/usr/local/stow/opencv-$OPENCV_VERSION
fi

# Print out the current configuration
echo "Build configuration: "
echo " Building for platform: ${PLATFORM}"
if [ "${INSTALL_DIR}" != "" ]; then
  echo " OpenCV binaries will be installed in: $INSTALL_DIR"
else
  echo " NOT installing OpenCV binaries"
fi
echo " OpenCV Source will be installed in: $SOURCE_DIR"
if [ "$PACKAGE_OPENCV" = "" ] ; then
   echo " NOT Packaging OpenCV"
else
   echo " Packaging OpenCV"
fi
if [ $DOWNLOAD_OPENCV_EXTRAS == "YES" ] ; then
 echo "Also downloading opencv_extras"
fi

# Remove and block existing OpenCV installs
$SUDO apt remove -y "libopencv*"

PYTHON3=$(which python3 | xargs readlink -e | xargs basename)
if [ "${PYTHON3}" == "" ]; then
  if [ "${PLATFORM}" == "jetpack" ]; then
  	PYTHON3="python3.6"
  else
    PYTHON3="python3.8"
  fi
fi

## Build configuration
WHEREAMI=$PWD

mkdir -p $SOURCE_DIR

# Repository setup
$SUDO apt-add-repository universe
$SUDO apt-get update

# Download dependencies for the desired configuration
cd $WHEREAMI
$SUDO apt-get install -y \
    software-properties-common \
    build-essential \
    cmake \
    file \
    libavcodec-dev \
    libavformat-dev \
    libavresample-dev \
    libavutil-dev \
    libeigen3-dev \
    libglew-dev \
    libgtk2.0-dev \
    libgtk-3-dev \
    libjpeg-dev \
    libpng-dev \
    libpostproc-dev \
    lib${PYTHON3}-dev \
    libswscale-dev \
    libtbb-dev \
    libtiff5-dev \
    libv4l-dev \
    libxvidcore-dev \
    libx264-dev \
    libopenjp2-7-dev \
    ${PYTHON3}-dev \
    qt5-default \
    zlib1g-dev \
    pkg-config \
    git \
    stow

if [ "${PLATFORM}" == "jetpack" ]; then
  # # Apply patch on Jetpack 4.x
  # # https://devtalk.nvidia.com/default/topic/1007290/jetson-tx2/building-opencv-with-opengl-support-/post/5141945/#5141945
  # cd /usr/local/cuda/include
  # $SUDO patch -N -p1 < $WHEREAMI'/patches/OpenGLHeader.patch'

  CUDA_ARCH_BIND="7.2"
  CUDA_EXTRA_ARGS="-D CUDA_ARCH_BIN=${ARCH_BIN} -D CUDA_ARCH_PTX=\"\""

  # ## Fix breaking issue with numpy support for python2.7 (hacky)
  # cd /usr/include/python2.7/numpy
  $SUDO patch -N -p1 < $WHEREAMI'/patches/numpy_import_array_retval_python27.patch'
fi

# # Python 2.7
# $SUDO apt-get install -y python-dev  python-numpy  python-py  python-pytest
# OPENCV_PYTHON2_ARGS="-D BUILD_opencv_python2=ON"

# Python 3.6 (always)
$SUDO apt-get install -y python3-dev python3-numpy python3-py python3-pytest \
           libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libceres-dev

cd $SOURCE_DIR
if [ ! -d opencv ]; then
  git clone --depth 1 --branch "$OPENCV_VERSION" https://github.com/opencv/opencv.git
else
  cd opencv && git checkout $OPENCV_VERSION
fi

cd $SOURCE_DIR
if [ ! -d opencv_contrib ]; then
  git clone --depth 1 --branch "$OPENCV_VERSION" https://github.com/opencv/opencv_contrib.git
else
  cd opencv_contrib && git checkout $OPENCV_VERSION
fi

if [ $DOWNLOAD_OPENCV_EXTRAS == "YES" ] ; then
 echo "Installing opencv_extras"
 # This is for the test data
 cd $SOURCE_DIR
 git clone https://github.com/opencv/opencv_extra.git
 cd opencv_extra
 git checkout -b v${OPENCV_VERSION} ${OPENCV_VERSION}
fi

if [ "${INSTALL_DIR}" != "" ]; then
  INSTALL_ARGS="-D CMAKE_INSTALL_PREFIX=${INSTALL_DIR}"
fi

# Create the build directory and start cmake
cd $SOURCE_DIR/opencv
mkdir -p build && cd build

echo $PWD
time cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D OpenGL_GL_PREFERENCE=GLVND \
      ${INSTALL_ARGS} \
      -D WITH_CUDA=ON \
      ${CUDA_EXTRA_ARGS} \
      -D ENABLE_FAST_MATH=ON \
      -D CUDA_FAST_MATH=ON \
      -D WITH_CUBLAS=ON \
      -D WITH_LIBV4L=ON \
      -D WITH_V4L=ON \
      -D WITH_GSTREAMER=ON \
      -D WITH_GSTREAMER_0_10=OFF \
      -D WITH_QT=ON \
      -D WITH_OPENGL=ON \
      ${OPENCV_PYTHON2_ARGS} \
      -D BUILD_opencv_python3=ON \
	    -D BUILD_TESTS=OFF \
      -D BUILD_PERF_TESTS=OFF \
      -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
      ${PACKAGE_OPENCV} \
      ../

if [ $? -eq 0 ] ; then
  echo "CMake configuration make successful"
else
  # Try to make again
  echo "CMake issues " >&2
  echo "Please check the configuration being used"
  exit 1
fi

NUM_JOBS=$(nproc)
time make -j$NUM_JOBS
if [ $? -eq 0 ] ; then
  echo "OpenCV make successful"
else
  # Try to make again; Sometimes there are issues with the build
  # because of lack of resources or concurrency issues
  echo "Make did not build " >&2
  echo "Retrying ... "
  # Single thread this time
  make
  if [ $? -eq 0 ] ; then
    echo "OpenCV make successful"
  else
    # Try to make again
    echo "Make did not successfully build" >&2
    echo "Please fix issues and retry build"
    exit 1
  fi
fi

if [ "$INSTALL_DIR" != "" ] ; then

  echo "Installing ... "
  $SUDO make install
  $SUDO ldconfig
  if [ $? -eq 0 ] ; then
    echo "OpenCV installed in: $INSTALL_DIR"
  else
    echo "There was an issue with the final installation"
    exit 1
  fi

  # Unlink existing version
  if [ -L /usr/local/lib/opencv4 ]; then
    # Probably more robust ways to do this
    EXISTING_OPENCV=`readlink /usr/local/lib/opencv4 | awk -F '/' '{print $3}'`

    if [ "$EXISTING_OPENCV" != "" ]; then
      echo "Un-stowing existing version $EXISTING_OPENCV"
      cd /usr/local/stow && $SUDO stow -D $EXISTING_OPENCV
    fi
  fi

  # Stow this new version
  cd /usr/local/stow && $SUDO stow -R opencv-$OPENCV_VERSION
  if [[ ! -L /usr/include/opencv ]]; then
    cd /usr/include && $SUDO ln -s /usr/local/include/opencv4 opencv
  fi

  # Create this .pth file for /usr/local/lib/pythonXX/site-packages to added
  # to python path in Ubuntu
  if [[ "$PYTHON3" != "" ]]; then
    $SUDO mkdir -p /usr/local/lib/${PYTHON3}/dist-packages/
  	$SUDO sh -c "echo \"/usr/local/lib/${PYTHON3}/site-packages\" > /usr/local/lib/${PYTHON3}/dist-packages/site-packages.pth"
  fi

  # check installation
  IMPORT_CHECK="$(python -c "import cv2 ; print cv2.__version__")"
  if [[ $IMPORT_CHECK != *$OPENCV_VERSION* ]]; then
    echo "There was an error loading OpenCV in the Python sanity test."
    echo "The loaded version does not match the version built here."
    echo "Please check the installation."
    echo "The first check should be the PYTHONPATH environment variable."
  fi
fi

# If PACKAGE_OPENCV is on, pack 'er up and get ready to go!
# We should still be in the build directory ...
#
# Generally we are not packaging anymore
#
# Either "make install"ing directly to bare metal
# Or using Docker multi-stage builds to copy from a build image to
# a runtime image.
#
if [ "$PACKAGE_OPENCV" != "" ] ; then
   echo "Starting Packaging"
   cd $SOURCE_DIR/opencv/build

   ## Update permissions to allow `make install` to run w/o sudo
   $SUDO chown ${USER} install_manifest*.txt

   $SUDO ldconfig
   time make package -j$NUM_JOBS
   if [ $? -eq 0 ] ; then
     echo "OpenCV make package successful"
   else
     # Try to make again; Sometimes there are issues with the build
     # because of lack of resources or concurrency issues
     echo "Make package did not build " >&2
     echo "Retrying ... "
     # Single thread this time
      make package
     if [ $? -eq 0 ] ; then
       echo "OpenCV make package successful"
     else
       # Try to make again
       echo "Make package did not successfully build" >&2
       echo "Please fix issues and retry build"
       exit 1
     fi
   fi
fi

cd $PWD
