#!/bin/bash
set -e

ros_env_setup="/opt/ros/$ROS_VERSION/setup.bash"
echo "sourcing   $ros_env_setup"
source "$ros_env_setup"

echo "ROS_ROOT   $ROS_ROOT"
echo "ROS_DISTRO $ROS_VERSION"

exec "$@"
