#!/usr/bin/env bash
set -e

source functions.sh
load_config

# source nv_secrets.sh

# if [ "${NVIDIA_PASSWD}" == "" ]; then
#     echo "Must create the \"nv_secrets.sh\" file"
#     exit
# fi

echo "Building base image"
docker build --network=host \
                --build-arg BASE_IMAGE=${L4T_BASE_IMAGE} \
                --build-arg PLATFORM=${PLATFORM} \
                -t ${BASE_IMAGE_LATEST} \
                -f docker/Dockerfile.00_base_image .

                # --build-arg NVIDIA_USER=${NVIDIA_USER} \
                # --build-arg NVIDIA_PASSWRD=${NVIDIA_PASSWD} \
