#!/usr/bin/env bash
set -e

source functions.sh
load_config

echo "Building Trisect runtime image"
docker build --network=host \
                --build-arg BASE_IMAGE=${ROS_RUNTIME_IMAGE} \
                --build-arg PLATFORM=${PLATFORM} \
                -t ${TRISECT_RUNTIME_IMAGE} \
                -t ${TRISECT_TAG_IMAGE} \
                -t ${TRISECT_LATEST_IMAGE} \
                -t ${GITLAB_TRISECT_RUNTIME_IMAGE} \
                -t ${GITLAB_TRISECT_TAG_IMAGE} \
                -t ${GITLAB_TRISECT_LATEST_IMAGE} \
                -f docker/Dockerfile.03_trisect_runtime .
