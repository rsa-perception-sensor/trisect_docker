# trisect_environment

Opinionated scripts for building a "standard" environment for GPU-accelerated stereo.   Originally developed for the "base" install on [Trisect](https://trisect-perception-sensor.gitlab.io/trisect-docs/), it now supports three endpoints:

- Bare-metal installation onto either an Xavier or Orin NX board running L4T, specifically the [Trisect](https://trisect-perception-sensor.gitlab.io/trisect-docs/) perception sensor
- In an ARM64 docker image to emulate the Trisect environment (building on an Nvidia L4T base image)
- In an AMD64 docker image to run Trisect-adjacent sofrware like [gpu_stereo_image_proc](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc) (building on NVidia Cuda images).

All installs build a recent OpenCV from source with optimizations, then build ROS1 linked against that veresion of openCV.

For Jetson bare-metal CUDA and various accelerated libraries (VPI, etc) are linked in via the jetson-docker mechanism.

For L4T docker, those libraries are included in the base image.  For AMD64 these are manually installed.

The Docker images **do not** install the Trisect s/w stack.

# Building on Trisect

Building on bare metal Trisect is largely a manually process:

```
cd buildOpenCV
./buildOpenCV.sh --platform xavier_nx (or orin)
cd ../buildROS1
./buildROS1 --platform xavier_nx (or orin)
```

# Building Docker images

By default the script auto-detects your Ubuntu version and architecture.  So:

```
./all_docker.sh
```

Will run through multiple build stages to:

1. Build a base image.
1. Build an OpenCV **build image.**
1. Install the resulting packages into a clean base image to make a OpenCV **runtime image.**
1. Use that as a base image for a **ROS build.**
1. Install the resulting packages into an OpenCV base image to make a ROS **runtime image.**
1. Install some additional Trisect-ish dependencies into the ROS runtime image.

The Dockerfile copies the local `buildOpenCV` and `buildROS1` directories (and any associated uncommitted changes) into the Dockerfile during the build for development.

The final images are labelled:

* `amarburg/trisect:latest`
* `amarburg/trisect:runtime`
* `amarburg/trisect:runtime-{git SHA hash}`



# License


Heavily inspired by [dustynv's jetson-containers package](https://github.com/dusty-nv/jetson-containers/) which is provided under the MIT licenseas follows:

```
Copyright (c) 2020, NVIDIA CORPORATION. All rights reserved.
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
```

----

----

----


# Previous documentation.

Scripts for building Docker images which contain the  OpenCV/ROS builds as the "native" Trisect.

To do this, it uses a sequence of steps, including building a number of intermediate Docker images.  The script [`all.sh`](all.sh) runs each of these steps in sequence.  Configuration for the build process (e.g. image names) are stored [`config.sh`](config.sh).

The steps are as follows:

## 1. Compile Opencv

**`build_opencv.sh`** builds `Dockerfile.build_opencv`.

This buildfile starts with an NVidia L4T base image and executes the script found in [`buildOpenCV`](https://gitlab.com/apl-ocean-engineering/jetson/buildopencv).   This builds a custom copy of OpenCV, packaging the output as a `tar.bz2`.   This tarball is copied out of the image to a local directory.

This Docker image is  "disposable" -- it's not intended to be a base image for other images.


## 2. Build an OpenCV runtime Docker image

**`opencv_runtime.sh`** builds `Dockerfile.opencv_runtime`.

This buildfiles starts with an NVidia L4T, installs the prerequisites for OpenCV, as well as the tarball built in step 1.  This creates the base image `trisect:opencv-{version}`.

## 3. Compile ROS

**`build_ros.sh`** builds `Dockerfile.build_ros`.

This buildfile starts with the `trisect:opencv-{version}` image and runs [`buildROS1`](https://gitlab.com/apl-ocean-engineering/jetson/buildros1), packaging the output as a `tar.bz2`.  This tarball is copied out of the image to a local directory.

This Docker image is  "disposable" -- it's not intended to be a base image for other images.

## 4. Build a ROS runtime Docker image

**`ros_runtime.sh`** builds `Dockerfile.ros_runtime`.

This buildfile starts with the `trisect:opencv-{version}` image, installs dependencies for ROS, and unpacks the tarball from the previous step.  This creates the base image `trisect:ros`

## 5. Build a Trisect-specific Docker image

**`trisect_runtime.sh`** builds `Dockerfile.trisect_runtime`.

This buildfiles starts with `trisect:ros` and adds additional dependencies.  This creates the base images `trisect:runtime` and `trisect:latest`
