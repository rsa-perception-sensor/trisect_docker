#!/usr/bin/env bash

## Do everything!

set -e

./docker/00_build_base_image.sh
./docker/01_build_opencv.sh
./docker/02_build_ros.sh
./docker/03_build_trisect_runtime.sh
