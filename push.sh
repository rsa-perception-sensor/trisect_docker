#!/usr/bin/env bash

set -e
source functions.sh
load_config

docker push ${TRISECT_RUNTIME_IMAGE}
docker push ${TRISECT_TAG_IMAGE}
docker push ${TRISECT_LATEST_IMAGE}

docker push ${GITLAB_TRISECT_RUNTIME_IMAGE}
docker push ${GITLAB_TRISECT_TAG_IMAGE}
docker push ${GITLAB_TRISECT_LATEST_IMAGE}
