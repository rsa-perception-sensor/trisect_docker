#!/bin/bash


load_config() {
    if [ "${CONFIG}" == "" ]; then
        ARCH=$(uname -i)

        source /etc/os-release

        CONFIG=${PWD}/config_${VERSION_ID}_${ARCH}.sh
    fi

    echo "Loading config ${CONFIG}"
    source ${CONFIG}
}
