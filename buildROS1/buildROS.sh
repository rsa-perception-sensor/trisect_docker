#!/bin/bash

#
# docker run --runtime nvidia -it --rm -w /workdir -v `pwd`:/workdir -v `pwd`/build-in-docker:/workdir/build nvcr.io/nvidia/l4t-base:r32.4.4 ./buildROS.sh
#

set -e

source /etc/os-release
if [ "${VERSION_ID}" == "20.04" ]; then
    ROS_VERSION=noetic
else
    echo "Can't figure out which ROS version to build"
    echo -1
fi

## Only use sudo if necessary
## The Jetson Docker images run as root and don't include sudo (?)
SUDO=''
if (( $EUID != 0 )); then
    SUDO='sudo'
fi

function usage
{
    echo "usage: ./buildROS.sh"
    echo "--platform         Platform {jetpack-5.1.4,docker-amd64}"
    echo "-h | --help  This message"
}

# Iterate through command line inputs
while [ "$1" != "" ]; do
    case $1 in
        --platform )            shift
                                PLATFORM=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ "$PLATFORM" == "" ]; then
  echo "Platform not defined.  Valid values:  jetpack-5.1.4, docker-amd64"
  exit -1
fi


TOPDIR=${PWD}
BUILD_ROS_WS=${PWD}/build
INSTALL_DIR=/opt/ros/$ROS_VERSION


## Install "normal" apt dependencies
$SUDO apt-get update

#!! Note we do not take any steps to remove/install OpenCV
#!1 OpenCV is assumed to have been installed/built.

## **Explicitly** remove any openCV dependencies.
## **MUST** use the compiled OpenCV
##$SUDO apt remove -y "libopencv*"
#
# \todo{} Check for OpenCV -- either from APT or from source...

export DEBIAN_FRONTEND=noninteractive

if [ ! -f /etc/localtime ]; then
    $SUDO ln -fs /usr/share/zoneinfo/UTC /etc/localtime
    $SUDO apt-get install -y tzdata
    $SUDO dpkg-reconfigure --frontend noninteractive tzdata
fi

# Get base requirements
$SUDO apt-get install -y curl build-essential git lsb-release \
            libgtk2.0-dev gpg-agent

## Enable ROS repository
$SUDO sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | $SUDO apt-key add -

## Install ROS apt dependencies
$SUDO apt-get update
if [[ $ROS_VERSION == "noetic" ]]; then
    $SUDO apt-get install -y python3-rosdep2 python3-rosinstall-generator \
                python3-vcstool python3-rosinstall python3-rospkg python3-empy
fi

# this command is allowed to return non-true
$SUDO rosdep init || true
rosdep update

mkdir -p $BUILD_ROS_WS/src && cd $BUILD_ROS_WS

vcs import src < $TOPDIR/${ROS_VERSION}-${PLATFORM}.rosinstall

## Patches which will impact rosdep...
set +e  # ... are allowed to fail
patch --forward -d src/camera_info_manager_py/ -p1 < $TOPDIR/patches/camera_info_manager_py_add_camera_info_manager_to_cmakelists.patch

# foxglove_bridge uses the
#
#   <depend condition="$ROS_VERSION == 1">...</depend>
#
# syntax, but the rosdep in melodic / Jetpack 4.5.1 doesn't support it
# echo "Patching foxglove_bridge"
# sed -i 's/condition="$ROS_VERSION == 1"//' src/foxglove_bridge/package.xml

# camera_info_manager_py has a rosdep for python-rospkg, which doesn't exist in noetic
echo "*** Patching camera_info_manager_py"
sed -i 's/<package>/<package format="2">/g' src/camera_info_manager_py/package.xml
sed -i 's/run_depend/exec_depend/g' src/camera_info_manager_py/package.xml
sed -i 's/python-/python3-/g' src/camera_info_manager_py/package.xml

set -e

# Surely this long list can't be right
SKIP_KEYS=" --skip-keys libopencv-dev \
            --skip-keys visualization_tutorials \
            --skip-keys rqt_gui \
            --skip-keys rqt_gui_cpp \
            --skip-keys python3-opencv \
            --skip-keys python-rospkg \
            --skip-keys common_tutorials \
            --skip-keys geometry_tutorials \
            --skip-keys ros_tutorials \
            --skip-keys urdf_tutorial "
rosdep install --verbose --from-paths src --ignore-src ${SKIP_KEYS} --rosdistro ${ROS_VERSION} -y

$SUDO mkdir -p ${INSTALL_DIR} || true
$SUDO chown -R `whoami` ${INSTALL_DIR} || true
./src/catkin/bin/catkin_make_isolated -DCATKIN_BLACKLIST_PACKAGES="rqt_rosmon" \
            --install-space ${INSTALL_DIR} --install -DCMAKE_BUILD_TYPE=Release
