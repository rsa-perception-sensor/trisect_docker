#!/usr/bin/bash

UBUNTU="20.04"

# Jetpack 5.1.2 uses Cuda 11.4.19 (which isn't even an official release?)
# CUDA 11.4.4 is the latest 11.4.x release, but hasn't been released as a Docker image
# ... oh-kay, how about 11.8.0, the latest 11.x release
CUDA="11.8.0"

PLATFORM="docker-amd64"

L4T_BASE_IMAGE=nvidia/cuda:${CUDA}-devel-ubuntu${UBUNTU}
DOCKER_REPO="amarburg/trisect"

## ~~ Base image ~~

BASE_IMAGE_LATEST="${DOCKER_REPO}:base-latest"

## ~~ OpenCV build/install configuration ~~

# As of 9/7/23,
# Need "4.x" branch to get this fix: https://github.com/opencv/opencv/pull/24104
# for Cuda 12.2
#
# Use tagged versions of OpenCV for earlier CUDA versions
#
OPENCV_VERSION="4.8.0"

OPENCV_BUILD_IMAGE="${DOCKER_REPO}:build-opencv-${OPENCV_VERSION}"
OPENCV_RUNTIME_IMAGE="${DOCKER_REPO}:opencv-${OPENCV_VERSION}"

## ~~ ROS build/install configuration ~~

BUILDROS_BRANCH="main"
ROS_VERSION="noetic"

ROS_BUILD_IMAGE="${DOCKER_REPO}:build-ros-${ROS_VERSION}"
ROS_RUNTIME_IMAGE="${DOCKER_REPO}:ros-${ROS_VERSION}"

## ~~ Full Trisect runtime configuration ~~

VERSION=$(git log -1 --pretty=%h)

TRISECT_TAG_IMAGE="${DOCKER_REPO}:runtime-$VERSION"
TRISECT_LATEST_IMAGE="${DOCKER_REPO}:latest"
TRISECT_RUNTIME_IMAGE="${DOCKER_REPO}:runtime"

GITLAB_REPO=registry.gitlab.com/rsa-perception-sensor/trisect_environment
GITLAB_TRISECT_TAG_IMAGE="${GITLAB_REPO}:runtime-$VERSION"
GITLAB_TRISECT_LATEST_IMAGE="${GITLAB_REPO}:latest"
GITLAB_TRISECT_RUNTIME_IMAGE="${GITLAB_REPO}:runtime"
