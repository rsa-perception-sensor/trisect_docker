#!/usr/bin/env bash

set -e

TRISECT_DOCKER_BRANCH=${TRISECT_DOCKER_BRANCH:-main}

source functions.sh
load_config

##========== Build OpenCV in a build docker image ================

DOCKER_BUILD_ARGS="\
        --network=host \
        --build-arg BASE_IMAGE=${BASE_IMAGE_LATEST} \
        --build-arg OPENCV_VERSION=${OPENCV_VERSION} \
        --build-arg PLATFORM=${PLATFORM}"

echo "Building OpenCV version ${OPENCV_VERSION}"
docker build ${DOCKER_BUILD_ARGS} \
                --target build_image \
                -t ${OPENCV_BUILD_IMAGE} \
                -f docker/Dockerfile.01_opencv .

##========== Install packages in a runtime image ================


echo "Building OpenCV runtime image"
docker build ${DOCKER_BUILD_ARGS} \
                --target runtime_image \
                -t ${OPENCV_RUNTIME_IMAGE} \
                -f docker/Dockerfile.01_opencv .


##
## Test
if [ "${PLATFORM}" == "xavier" ]; then
    docker run --rm -it ${OPENCV_RUNTIME_IMAGE} \
                python2 -c "import cv2; print cv2.__version__"
fi

docker run --rm -it  --runtime=nvidia  ${OPENCV_RUNTIME_IMAGE} \
                python3 -c "import cv2; print(cv2.__version__)"
