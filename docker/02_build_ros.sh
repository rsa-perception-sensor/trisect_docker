#!/usr/bin/env bash
set -e

source functions.sh
load_config

DOCKER_BUILD_ARGS="\
            --network=host \
            --build-arg BASE_IMAGE=${OPENCV_RUNTIME_IMAGE} \
            --build-arg ROS_VERSION=${ROS_VERSION} \
            --build-arg PLATFORM=${PLATFORM} "

##========== Build ROS in a build docker image ================

echo "Building ROS"
docker build ${DOCKER_BUILD_ARGS} \
                --target build_image \
                --build-arg BASE_IMAGE=${OPENCV_RUNTIME_IMAGE} \
                -t ${ROS_BUILD_IMAGE} \
                -f docker/Dockerfile.02_ros .

##========== Install packages in a runtime image ================

echo "Building ROS runtime image ${ROS_VERSION}"
docker build ${DOCKER_BUILD_ARGS} \
                --target runtime_image \
                -t ${ROS_RUNTIME_IMAGE} \
                -f docker/Dockerfile.02_ros .

##
## Brief tests
if [[ "${ROS_VERSION}" == "noetic" ]]; then
    docker run --rm -it ${ROS_RUNTIME_IMAGE}  \
                    python3 -c "import rospy; print(rospy)"
else
    docker run --rm -it ${ROS_RUNTIME_IMAGE}  \
                    python -c "import rospy; print rospy"
fi
